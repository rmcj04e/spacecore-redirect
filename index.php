<?php

use Rmcj\SpacecoreRedirect\RedirectController;

require __DIR__."/config.php";
require __DIR__."/vendor/autoload.php";

$rc = new RedirectController();

if(empty(REDIRECT_URL) || empty(REQUEST_DATA) || empty(REQUEST_METHOD)) exit('Укажите параметры перенаправления в config.php!');

switch(REQUEST_METHOD)
{
  case "GET":
    $urlWithQueryString = REDIRECT_URL."?";

    foreach(REQUEST_DATA as $key => $item){        
        $urlWithQueryString.="{$key}={$item}&";
    }

    header("Location: {$urlWithQueryString}");
    break;
  case "POST":
    $rc->invokeRedirectWithPOST(REDIRECT_URL, REQUEST_DATA);
    break;
  default:
    exit("Метод, указаный вами в config.php на данный момент не предусмотрен. Доступны: GET, POST");
    break;   
}
