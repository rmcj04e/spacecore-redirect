<?php

namespace Rmcj\SpacecoreRedirect;

class POSTHelper {

  public $redirectURL;
  public $params;
  public $inputs;

  public function __construct(){
  }

  public function composeHTML($redirectURL, $params){
    
      $this->redirectURL = $redirectURL;
      $this->params = $params;

      $this->generateInputs();
      $form = $this->generateForm();
      $finalHTML = $this->addJStoForm($form);
      return $finalHTML;
  }

  public function generateForm(){
    $htmlAfterInputs = "<form action='{$this->redirectURL}' method='post' id='redirectForm'>";
    foreach($this->inputs as $input){
      $htmlAfterInputs.=$input;
    }
    return $htmlAfterInputs.="</form>";
  }

  public function addJStoForm($form){
    $js = "<script> setTimeout(() => { let form = document.getElementById('redirectForm'); form.submit(); }, 1500)</script>";
    return $form.=$js;
  }
  public function generateInputs(){
    $inputs = [];
    
    foreach($this->params as $key => $value){
      array_push($inputs, "<input type='hidden' name='{$key}' value='{$value}'>");
    }
    $this->inputs = $inputs;

    return true;
  }
}
