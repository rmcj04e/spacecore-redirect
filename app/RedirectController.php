<?php

namespace Rmcj\SpacecoreRedirect;

use Rmcj\SpacecoreRedirect\POSTHelper;

class RedirectController {

  public $postHelper;

  public function __construct()
  {
    $this->postHelper = new POSTHelper();
  }

  public function invokeRedirectWithGET($redirectURL, $params)
  {

  }

  public function invokeRedirectWithPOST($redirectURL, $params)
  {
    $html = $this->postHelper->composeHTML($redirectURL, $params);
    exit($html);
  }
}
